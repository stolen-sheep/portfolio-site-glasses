<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$name = $_POST['name'];
$email = $_POST['email'];
$message = $_POST['message'];

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Form Submission | Bill Mauro Design</title>
  <meta name="description" content="Bill Mauro Design">
  <meta name="author" content="Bill Mauro Jr. | Stolen Sheep Design">
  <link href="https://fonts.googleapis.com/css?family=Oxygen+Mono" rel="stylesheet">
  <link rel="stylesheet" href="css/styles.css">
</head>
<body class="about">

<nav id="primary-nav">
    <a class="nav-item-1" href="index.html">Home</a>
    <a class="nav-item-2" href="work.html">Work</a>
    <a class="nav-item-3" href="javascript:void(0);">About</a>
    <a class="nav-item-4" href="contact.php">Contact</a>
</nav>

<main>
    <?php echo 'name = ' . $name . '<br>' . 'email = ' . $email . '<br>' . 'message = ' . $message; ?>
</main>
<footer>
    <p>&copy; 2019 BILL MAURO JR. / STOLEN SHEEP DESIGN</p>
</footer>
  <!-- <script src="js/scripts.js"></script> -->
</body>
</html>
