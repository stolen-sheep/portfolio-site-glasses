$(document).ready(function(){

    console.log('you get outta here');

    var portfolioItem = $('.portfolio-item');
    var win = $(window);
    var winHeight = win.height();
    var glassesSection = $('#portfolio-header');

    glassesSection.height(winHeight);

    portfolioItem.on('click touch', function(e){

        e.preventDefault();

        var $this = $(this);
        var pid = $this.data('pid');
        console.log(pid);

        window.location.href = "/portfolio/project.html?pid=" + pid;

    });

});
