//namespace
var app = {
  templates: {},
  projects: {}
}

//template for lodash use
app.files = [{
  name: "project",
  file: "project.inc.html"
}];

$(document).ready(function() {

    // variabibbles
	var urlParam = window.location.search;
	var pid = urlParam.substring(5); // rm ?pid= and keep the index, single or double digit
    app.pid = parseInt(pid);
    var win = $(window);
	var animationHeight;
	var winHeight = win.height();
    var winWidth  = win.width();
	var scrollTop;
    var templatesLoaded = false;
    var projectsLoaded = false;
    var video = document.getElementById('projectVideo');
    var vidPlay = $("#vidPlay");
    var vidPause = $("#vidPause");
    var vidToggle = $("#vidToggle");

    // pull in project json for page build
	$.getJSON("projects.json", function (resp) {
		app.projects = resp;
        projectsLoaded = true;
        //console.log('Step 1: projectsLoaded = ' + projectsLoaded);
	}).fail(function (resp) {
		console.warn("Got error from config load", resp);
	});

    app.init = function() {
        app.loadTemplates();
    }

	// if the window scrolls past the bottom of the animation, reformat the layout
	win.on('scroll', function(){

        animationHeight = $('.project_animation').height();
        winWidth = win.width();

        if (winWidth < 1280) {
            $('.project-details').removeClass('fixed');
        }

		var scrollTop = win.scrollTop();

		if ( scrollTop > animationHeight && winWidth >= 1280 ) {
			$('.project-details').addClass('fixed');
		} else {
			$('.project-details').removeClass('fixed');
		}

	});

    app.populateProject = function(){
        if( projectsLoaded && templatesLoaded) {
            $('.project-view').html(app.templates.project);
            video = document.getElementById('projectVideo');
            $('body').on("click touch", function(e){
                var target = e.target.id;
                console.log(target);
                if ( target == "vid-pause") {
                    e.preventDefault();
                    video.pause();
                } else if ( target == "vid-play" ) {
                    e.preventDefault();
                    video.play();
                } else if ( target == "vid-toggle" ) {
                    e.preventDefault();
                    if(video.muted) {
                        video.muted = !video.muted;
                    } else {
                        video.muted = true;
                    }

                } else {
                    //
                }
            });
        }
    }

    // app.showControls = function(){
    //     if (winWidth <= 1028  && $("#project-video").controls == false ){
    //         $("#project-video").controls = true;
    //     }
    // }

    // increment through app.files and create a lodash for each
    app.loadTemplates = function(cb) {

        var loaded = 0;
        _.each(app.files, function(f) {
            //console.log("Loading: ", f.file);

            $.get("inc/" + f.file, function(html) {
                app.templates[f.name] = _.template(html);
                loaded++;

                if (loaded === app.files.length) {
                    //console.log("Finished Loading Templates: " + loaded + " of " + app.files.length);
                    templatesLoaded = true;
                    //console.log('Step 2: templatesLoaded = ' + templatesLoaded);
                    app.populateProject();
                    //    app.showControls();
                }
            });

        });

    }; //-loadTemplates





    app.init();


});
