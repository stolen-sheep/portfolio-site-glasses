// THIS FILE REQURES THAT THE LIB FILE IS LOADED FIRST


if (!_.isObject(app)) {
  alert("System Library failed to load [SYSLD01]. Pleasr report this error to support@nthriveeducation.com");
  //the app object is defined in lib.js
}



if (app.dev) {
  app.log("DEV MODE ON");
}

app.files = [{
  name: "default",
  file: "default.inc.html"
}];

$(document).ready(function() {
  $('.adminon').hide();
  app.setListeners();
  app.init(function() {
    app.startApp();
  });
});

app.setListeners = function() {

  app.setLibListeners();

      $(document).on('click touch', '.btn-view', function(event){

          event.preventDefault();

          // element attributes are stored in 'data-object' as a JSON object;
          // retreive the parent with the data object and parse for attributes
          // window.fileObject = JSON.parse($(this).closest('.card').attr('data-object'));

          app.buildMediaObject(event);

          app.PopupCenter('content.html', 'mediapopup', 1024,768);

          //lrs.experienced();

       }); // on('click touch', '.btn-view'


       $(document).on('submit', '.search-form', function(e) {
        e.preventDefault();
        var targetField = $(e.target).attr('data-field');
        var query = $('#'+targetField).val().toString();
        if(query.length > 0){
            app.buildSearchResults(query);
        }else{
            return false;
        }
    });


} //setListeners

app.proceedToBuild = function() {
    app.buildDefault();
	//lrs.init();
}

app.PopupCenter = function(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'location=no,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no,fullscreen=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    //app.log(newWindow.object);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}

// app.initModals = function(){
// 	var elems = document.querySelectorAll('.modal');
//     var instances = M.Modal.init(elems, {
// 		onCloseEnd: function(){ $(".modal-content").html(""); }
// 	});
// }

app.buildDefault = function() {

    $('#maincontainer').fadeOut(function(){
        $('#maincontainer').html(app.templates.default).fadeIn();
        $('.collapse').collapse();
    });

};

app.buildSearchResults = function(query) {

    var searchTerms = query.toLowerCase().split(" ");
    var searchResults = app.config;

    _.forEach(searchResults.sections, function(section, s) {

            _.forEach(section.categories, function(category, c) {

                category.links = [];

                _.forEach(searchTerms, function(term, t){

                    if (_.indexOf(app.config.sections[s].categories[c].links, ))

                });

            }); // .forEach categories

        }); // .forEach sections

    });

    app.log('search results: ', searchResults);

    $('#maincontainer').fadeOut(function(){
        $('#maincontainer').html(app.templates.loader).fadeIn();
        $('.collapse').collapse();
    });

};

app.buildMediaObject = function(event) {

    var target     = $(event.target);
    var linkId     = target.closest('.card').attr('id');
    var categoryId = target.closest('.collapse').attr('id');
    var sectionId  = target.closest('.accordion').attr('id');

    // window.mediaObject is identical to the current section object from config.json
    window.mediaObject = _.find(app.config.sections, ['sectionId', sectionId]);
    // creates a filtered array containing one object called "categories"
    window.mediaObject.categories = _.filter(window.mediaObject.categories, ['categoryId', categoryId]);
    // creates a filtered array containing one object called "links"
    window.mediaObject.categories[0].links = _.filter(window.mediaObject.categories[0].links, ['linkId', linkId]);

    //app.log(window.mediaObject);

};

app.startApp = function() {
  app.log("Starting");

  $.getJSON("config.json", function (resp) {
      app.config = resp;
      app.proceedToBuild();
      //app.log(resp);
  }).fail(function (resp) {
      app.warn("Got error from config load", resp);
  });
  // app.initModals();

} //- startApp




app.loadTemplates = function() {

		  app.templates.modal = _.template("<div class='modal fade' tabindex='-1' role='dialog' id='modal'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title'><%=title%></h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'><%=content%></div><div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
          app.templates.loader = "<div class='d-block row w-100' id='loader'></div>";
          app.templates.loaderSmall = "<div class='d-block row w-100 text-center' id='loaderSmall'><img src='images/site/preloader.gif' /></div>";

} //- loadTemplates
