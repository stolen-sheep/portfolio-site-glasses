<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * This example shows how to handle a simple contact form.
 */
//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;

require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/Exception.php';
$msg = '';
//Don't run this unless we're handling a form submission
if (array_key_exists('email', $_POST)) {
    date_default_timezone_set('Etc/UTC');
    //require '../vendor/autoload.php';
    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    //Tell PHPMailer to use SMTP - requires a local mail server
    //Faster and safer than using mail()
    $mail->isSMTP();
    $mail->Host = 'localhost';
    $mail->Port = 25;
    //Use a fixed address in your own domain as the from address
    //**DO NOT** use the submitter's address here as it will be forgery
    //and will cause your messages to fail SPF checks
    $mail->setFrom('bill@stolen-sheep.com', 'First Last');
    //Send the message to yourself, or whoever should receive contact for submissions
    $mail->addAddress('wmaurojr@gmail.com', 'bill mauro');
    //Put the submitter's address in a reply-to header
    //This will fail if the address provided is invalid,
    //in which case we should ignore the whole request
    if ($mail->addReplyTo($_POST['email'], $_POST['name'])) {
        $mail->Subject = 'PHPMailer contact form';
        //Keep it simple - don't use HTML
        $mail->isHTML(false);
        //Build a simple message body
        $mail->Body = <<<EOT
Email: {$_POST['email']}
Name: {$_POST['name']}
Message: {$_POST['message']}
EOT;
        //Send the message, check for errors
        if (!$mail->send()) {
            //The reason for failing to send will be in $mail->ErrorInfo
            //but you shouldn't display errors to users - process the error, log it on your server.
            $msg = 'Sorry, something went wrong. Please try again later.';
        } else {
            $msg = 'Message sent! Thanks for contacting us.';
        }
    } else {
        $msg = 'Invalid email address, message ignored.';
    }
}
?>
<!doctype html>
<html lang="en" id="contact-page">
<head>
  <meta charset="utf-8">
  <title>Contact | Bill Mauro Design</title>
  <meta name="description" content="Bill Mauro Design">
  <meta name="author" content="Bill Mauro Jr. | Stolen Sheep Design">
  <link href="https://fonts.googleapis.com/css?family=Oxygen+Mono" rel="stylesheet">
  <link rel="stylesheet" href="css/styles.css">
</head>
<body class="contact">

<nav id="primary-nav">
    <a class="nav-item-1" href="index.html">Home</a>
    <a class="nav-item-2" href="work.html">Work</a>
    <a class="nav-item-3" href="about.html">About</a>
    <a class="nav-item-4" href="javascript:void(0);">Contact</a>
</nav>

<main>
<?php if (!empty($msg)) {
    echo "<h2>$msg</h2>";
} ?>
    <section>
        <form method="POST" action="test.php">
            <h1>Contact</h1>
            <label for="name">Name:<br><input type="text" name="name" id="name"></label><br>
            <label for="email">Email address:<br><input type="email" name="email" id="email"></label><br>
            <label for="message">Message:<br><textarea name="message" id="message" rows="8" cols="20"></textarea></label><br>
            <input type="submit" value="Send">
        </form>
    </section>
</main>
<footer>
    <p>&copy; 2019 BILL MAURO JR. / STOLEN SHEEP DESIGN</p>
</footer>
  <!-- <script src="js/scripts.js"></script> -->
</body>
</html>
